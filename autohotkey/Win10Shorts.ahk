﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#InstallKeybdHook
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force

; Define the group: Windows Explorer windows
GroupAdd, Explorer, ahk_class CabinetWClass

; Rename a file when alt-shift-r is pressed in windows explorer
~+!r::Gosub renameIfExplorer         ; Calls the rename subroutine. "!" is alt, ~ kbhook, + is shift

; Our subroutine for renaming in windows explorer
RenameIfExplorer:
    WinGetActiveTitle, winTitle         ; The title of the window is used to
    WinGetClass, winClass, %winTitle%   ; get its class.

    if (winClass = "CabinetWClass")     ; Windows classes windows explorer as CabinetWClass
    {
        Send {F2}
    }
Return  ; end RenameIfExplorer

#InstallKeybdHook       ; Grab keys before windows does

#w:: Run, "C:\Program Files\Mozilla Firefox\firefox.exe"
+#c::Run, "C:\Windows\System32\calc.exe"


; my keyboard has hardware programming that sends the caps lock key as escape. This would be some kung fu otherwisea
#c::Gosub openExplorerItemInCode       ; '#' translates to windows key. ~ allows the hotkey to cascade to windows after we used it.
; Can't use alt-shift-c till i limit it to desktop and explorer.
;~+!c::Gosub openExplorerItemInCode       ; '#' translates to windows key.~ allows the hotkey to cascade to windows after we used it, + is shift, ! is alt.

openExplorerItemInCode:
    ;WinGetActiveTitle, winTitle         ; The title of the window is used to
    ;WinGetClass, winClass, %winTitle%   ; get its class.

    ;if (winClass = "CabinetWClass")     ; Windows classes windows explorer as CabinetWClass
    ;{
        ; CURRENTLY CRASHY because sticks on clipWait
        ; if we try on say an explorer panel, hoping it will
        ; know to give us the current open folder address. ctrl-c
        ; doesn't give that.
        ; Want to test if a folder or file is selected, and if not
        ; just open code. Also maybe a timeout for clipwait.
         ;Run C:\Program Files\Microsoft VS Code\Code.exe" 
         Clipboard =
        Send ^c
        ClipWait ;waits for the clipboard to have content
        Run, "C:\Program Files\Microsoft VS Code\Code.exe" `"%clipboard%`"
    ;}

    ; Later we can try making this specific to windows explorer and desktop. 
    ; Desktop id:
    ;MouseGetPos , , , ID
	;WinGetTitle T, ahk_id %ID%
	;WinGetClass C, ahk_id %ID%
	;WinGet P, ProcessName, ahk_id %ID%
    ; and you're looking for Desktop_window:="Program Manager ahk_class Progman"
    ; more info: https://autohotkey.com/board/topic/79892-solved-unique-id-of-windows-desktop/
Return  ; end openExplorerItemInCode

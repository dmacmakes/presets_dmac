﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance force
#Warn ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

>#j::Send {Left}
+>#j::Send +{Left}
^+>#j::Send ^+{Left}

RWin::return

>#l::Send {Right}
+>#l::Send +{Right}
^+>#l::Send ^+{Right}

>#i::Send {Up}
+>#i::Send +{Up}
^+>#i::Send ^+{Up}

#>k::Send {Down}
+>#k::Send +{Down}
^+>#k::Send ^+{Down}

>#1::Send {F1}
>#2::Send {F2}
>#3::Send {F3}
>#4::Send {F4}
>#5::Send {F5}
>#6::Send {F6}

>#-::Send {Volume_Down}
>#=::Send {Volume_Up}

>#p::Send {PrintScreen}

CapsLock::Send {Escape}

~CapsLock & s::
if (GetKeyState("Shift","p") && GetKeyState("Ctrl","p")) {  
 	Send +^{Left}
} else if GetKeyState("Ctrl","p") {
	Send ^{Left}
} else if GetKeyState("Shift","p") {
	Send +{Left}
} else {
	Send {Left}
}

Return

~CapsLock & 1::Send ^{F5}
~CapsLock & 2::Send {F5}
~CapsLock & 3::Send {F10}
~CapsLock & 4::Send {F4}
~CapsLock & 5::Send {F5}
~CapsLock & 6::Send {F6}
;~CapsLock & s::Send {Left}
;~CapsLock & s::Send {Left}
~CapsLock & d::Send {Down}
~CapsLock & e::Send {Up}
~CapsLock & f::Send {Right}
~CapsLock & z::Send {BackSpace}
~CapsLock & x::Send {Delete}
~CapsLock & j::Send {Left}
~CapsLock & k::Send {Down}
~Escape & i::Send {Up}
~CapsLock & l::Send {Right}
~CapsLock & r::Send {End}
~CapsLock & w::Send {Home}
~CapsLock & -::Send {Volume_Down}
~CapsLock & =::Send {Volume_Up}
~CapsLock & v::Send {Volume_Up}
~CapsLock & c::Send {Volume_Down}
~CapsLock & p::Send {PrintScreen}





>#x::Send {Delete}
>#z::Send {Backspace}

>#h::Send {Home}
+>#h::Send +{Home}
+^>#h::Send +^{Home}

>#;::Send {End}
+>#;::Send +{End}
^+>#;::Send ^+{End}